# FFF-Discord-Bot
## About
This bot is develoed for the FFF Germany Discord Server.

## Features
* Role Self-giving
    * Trustable Roles
* ban and kick
* FFF info commands
* Botinfo message cahngeable by command

## Setup
### Docker

```
docker run \
-d \
--name fff-discord-bot \
--restart always \
-v fff-discord-bot-config:/usr/src/app/config \
-e ADMIN_ROLE="Bot Admin" \
-e TOKEN="YOURTOKEN" \
-e GUILD_ID=547480635720335360 \
registry.gitlab.com/fff-bots/fff-discord-bot
```
