import discord
from config import read_config


class NonMainServer(discord.ext.commands.CommandError):
    pass


discord.ext.commands.NonMainServer = NonMainServer


class checks_class:
    def __init__(self, bot):
        self.bot = bot

    def check_if_is_main_server(self, ctx):
        if ctx.guild.id == int(read_config("env", "GUILD_ID")):
            return True
        else:
            raise discord.ext.commands.NonMainServer
