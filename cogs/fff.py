import discord
from discord.ext import commands


class FFF(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def termine(self, ctx):
        """
        Sendet einen Link zu der Website mit Terminen
        """
        async with ctx.channel.typing():
            await ctx.send("Hier ist eine Liste aller Streiktermine:\nhttps://fridaysforfuture.de/streiktermine/")

    @commands.command()
    async def link(self, ctx):
        """
        Sendet den Link zu diesem Server
        """
        async with ctx.channel.typing():
            await ctx.send("Hier ist der Link zum offiziellen Discord Server von FFF DE:\nhttps://discord.gg/UEGyyrT")

    @commands.command()
    async def tg(self, ctx):
        """
        Schickt Links zu allen Bundesweiten Tg Gruppen
        """
        embed = discord.Embed(title="Telegram Gruppen", description=
        "Fridays for Future Germany Chats:\n"
        "• ☀️ Fridays for Future Germany ☀️ (https://t.me/FridaysForFutureDE)\n"
        "• FFF Diskussion (https://t.me/fffDiskussion)\n"
        "• FFF Politik (https://t.me/FFFPolitik)\n"
        "• FFF Energie (https://t.me/fffEnergie)\n"
        "• FFF Mobilität (https://t.me/fffMobilitaet)\n"
        "• FFF Ernährung (https://t.me/fffErnaehrung)\n"
        "• FFF Gesellschaft (https://t.me/fffGesellschaft)\n"
        "• FFF Kommunikation (https://t.me/fffGesellschaft)\n"
        "• FFF Soziale Frage (https://t.me/fffSozialeFrage)\n"
        "• FFF Verpackungen (https://t.me/fffVerpackungen)\n"
        "• FFF Wissenschaft (https://t.me/FFFWissenschaft)\n"
        "• FFF Sonstiges (https://t.me/fffSonstiges)\n"
        "• oFFF - Topic (https://t.me/fffofftopic)\n",
                              color=0x00ff00)
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(FFF(bot))
