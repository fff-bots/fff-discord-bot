from discord.ext import commands
import discord
from checks import checks
from loghelper import log
from config import read_config, write_config


class Selfrole(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        try:
            read_config("roles.json", "self_giveable_roles")
        except KeyError:
            write_config("roles.json", "self_giveable_roles", [])

    @commands.command()
    @commands.guild_only()
    @commands.check(checks.check_if_is_main_server)
    async def iam(self, ctx, role):
        """
        Gibt dir eine selbst-gebbare Rolle.
        """
        async with ctx.channel.typing():
            if role in read_config("roles.json", "self_giveable_roles"):
                for i in ctx.guild.roles:
                    if i.name == role:
                        await ctx.author.add_roles(i)
                await ctx.send("Du hast jetzt die {} Rolle".format(role))
            else:
                await ctx.send("Diese Rolle kannst du dir nicht selbst geben.")

    @commands.command()
    @commands.guild_only()
    @commands.check(checks.check_if_is_main_server)
    async def iamn(self, ctx, role):
        """
        Nimmt dir eine selbst-gebbare Rolle
        """
        async with ctx.channel.typing():
            if role in read_config("roles.json", "self_giveable_roles"):
                for i in ctx.guild.roles:
                    if i.name == role:
                        await ctx.author.remove_roles(i)
                await ctx.send("Du hast jetzt nicht mehr die {} Rolle.".format(role))

    @commands.command()
    @commands.guild_only()
    @commands.check(checks.check_if_is_main_server)
    async def lsar(self, ctx):
        """
        Listet alle selbst-gebbaren Rollen auf.
        """
        async with ctx.channel.typing():
            embed = discord.Embed(title="Selbst-gebbare Rollen")
            message = ""
            for role in read_config("roles.json", "self_giveable_roles"):
                message = message + role + "\n"
            embed.add_field(name="Diese Rollen kannst du dir selbst geben", value=message)
            embed.set_footer(
                text='Benutze !iam <Rolle> um dir eine Rolle zu geben, !iamn um sie dir wieder wegzunehmen"')
            await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    @commands.check(checks.check_if_is_main_server)
    @commands.has_role(read_config("env", "ADMIN_ROLE"))
    async def trustrole(self, ctx, role):
        """
        Fügt eine Rolle den selbst-gebbaren Rollen hinzu.
        """
        current_trusted_roles = read_config("roles.json", "self_giveable_roles")
        if role not in current_trusted_roles:
            roles_that_do_exist = []
            for i in await ctx.guild.fetch_roles():
                roles_that_do_exist.append(i.name)
            print(roles_that_do_exist)
            if role not in roles_that_do_exist:
                await ctx.guild.create_role(name=role, reason="Automatic self-giveable role-creation")
            current_trusted_roles.append(role)
            write_config("roles.json", "self_giveable_roles", current_trusted_roles)
            await ctx.send('{} wurde als selbst-gebbare Rolle hinzugefügt.'.format(role))
            await log(self.bot, title="Rolle selbst-gebbar gemacht", user=ctx.author,
                      fields=[{"title": "Rolle", "content": role}])
        else:
            await ctx.send("Rolle ist bereits selbst-gebbar.")

    @commands.command()
    @commands.guild_only()
    @commands.check(checks.check_if_is_main_server)
    @commands.has_role(read_config("env", "ADMIN_ROLE"))
    async def untrustrole(self, ctx, role):
        """
        Entfernt eine selbst-gebbare Rolle.
        """
        current_trusted_roles = read_config("roles.json", "self_giveable_roles")
        if role in current_trusted_roles:
            current_trusted_roles.remove(role)
            write_config("roles.json", "self_giveable_roles", current_trusted_roles)
            await ctx.send('{} wurde von den selbst-gebbaren Rollen entfernt.'.format(role))
            await log(self.bot, title="Rolle wurde von den selbst-gebbaren Rollen entfernt", user=ctx.author,
                      fields=[{"title": "Rolle", "content": role}])
        else:
            await ctx.send(
                "Rolle wurde nicht zu den selbst-gebbaren Rollen hinzugefügt, kann daher auch nicht entfernt werden.")


def setup(bot):
    bot.add_cog(Selfrole(bot))
