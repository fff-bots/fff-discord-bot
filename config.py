import json
import os

VERSION = "1.1"


def get_config_object(file):
    with open("config/config/" + file, "r") as config_file:
        try:
            return json.load(config_file)
        except:
            return {}


def set_config_object(file, object):
    with open("config/config/" + file, "w") as config_file:
        json.dump(object, config_file)


def read_config(file, setting):
    if file == "env":
        return os.environ.get(setting)
    if os.path.isfile("config/config/" + file) is False:
        with open("config/config/" + file, "w") as config_file:
            config_file.write("{}")
    with open("config/config/" + file, "r") as config_file:
        return json.load(config_file)[setting]


def write_config(file, setting, value):
    configuration = get_config_object(file)
    configuration[setting] = value
    set_config_object(file, configuration)
