import discord
from datetime import datetime

from config import read_config


async def log(bot, fields, title="", user=None, color=0):
    embed = discord.Embed(title=title, color=color)
    timestamp = datetime.now()
    timestamp = timestamp.replace(hour=timestamp.hour - 2)
    embed.timestamp = timestamp
    for i in fields:
        embed.add_field(name=i["title"], value=i["content"])
    if user is not None:
        embed.set_footer(text=f'{user.name} ({user.id})', icon_url=user.avatar_url)
    await bot.get_channel(int(read_config("env", "LOG_CHANNEL_ID"))).send("", embed=embed)
