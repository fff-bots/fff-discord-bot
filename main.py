#! /usr/bin/env python3
import asyncio
import logging
import sys
from pathlib import Path

from dotenv import load_dotenv

load_dotenv()

import discord
from discord.ext import commands

import checks
from config import read_config


def showprogress(amount, of):
    print(f"\r{amount}/{of}", end="")


async def run():
    """
    Where the bot gets started.
    """

    bot = Bot(description="Ich bin der Offizielle FFF Deutschland Discord Bot. Ich kann Informationen senden"
                          " und den Admins helfen.")
    try:
        if read_config("env", "TOKEN"):
            await bot.start(read_config("env", "TOKEN"))
        else:
            print("The variable TOKEN musst be defined either in .env or the environment!", file=sys.stderr)
            await bot.logout()
            sys.exit(2)

    except KeyboardInterrupt:
        await bot.logout()


class Bot(commands.Bot):
    def __init__(self, **kwargs):
        super().__init__(
            command_prefix=self.get_prefix_,
            description=kwargs.pop('description')
        )
        self.start_time = None
        self.app_info = None
        # self.uidhashcache = {}

        self.loop.create_task(self.load_all_extensions())

        # Add classes to self
        checks.checks = checks.checks_class(self)

        # Load list of blacklisted users
        rawblacklist = read_config("env", "BLACKLIST")
        self.blacklist = []
        if rawblacklist:
            for userid in read_config("env", "BLACKLIST").split(","):
                try:
                    self.blacklist.append(int(userid))
                except:
                    print(f"Error in blacklist: {userid} is not a valid integer!\n", file=sys.stderr)
                    sys.exit(2)

    async def get_prefix_(self, bot, message):
        """
        A coroutine that returns a prefix.

        I have made this a coroutine just to show that it can be done. If you needed async logic in here it can be done.
        A good example of async logic would be retrieving a prefix from a database.
        """
        prefix = ['!']
        return commands.when_mentioned_or(*prefix)(bot, message)

    async def load_all_extensions(self):
        """
        Attempts to load all .py files in /cogs/ as cog extensions
        """
        await self.wait_until_ready()
        await asyncio.sleep(1)  # ensure that on_ready has completed and finished printing
        # Intitialise some variables
        app_info = await self.application_info()
        self.owner = app_info.owner
        # Get list of cogs
        cogs = [x.stem for x in Path('cogs').glob('*.py')]
        # Always load the admin cog first
        try:
            cogs.remove("admin")
            cogs.insert(0, "admin")
        except ValueError:
            pass
        print(f"Loading {len(cogs)} extentions...")
        successes = ""
        for extension in cogs:
            try:
                self.load_extension(f'cogs.{extension}')
                successes += f'Loaded: {extension}\n'
            except Exception as e:
                error = f'{extension}\n {type(e).__name__} : {e}'
                print(f'Could not load extension: {error}\n', file=sys.stderr)
        print(successes, end="")
        print('-' * 10)

    async def on_ready(self):
        """
        This event is called every time the bot connects or resumes connection.
        """
        print('-' * 10)
        self.app_info = await self.application_info()
        print(f'Logged in as: {self.user.name}\n'
              f'Discord.py version: {discord.__version__}\n'
              f'Bot owner: {self.app_info.owner}')
        print('-' * 10)

    async def on_message(self, message):
        """
        This event triggers on every message received by the bot. Including one's that it sent itself.
        """
        # Ignore all bots and blacklisted users
        if message.author.bot or message.author.id in self.blacklist:
            return
        # Process the message
        await self.process_commands(message)

    async def on_message_edit(self, before, after):
        message = after
        # Ignore all bots and blacklisted users
        if message.author.bot or message.author.id in self.blacklist:
            return
        # Process the message
        await self.process_commands(message)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
